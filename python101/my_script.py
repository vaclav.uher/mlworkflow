from my_module import greet
from my_package.math import subtraction
from my_package.text import uppercase

name = 'Leonardo'
birthday = 1452
greet(name)
print('I am script and I am directly executable.')
print('Your age is {} or so.'.format(subtraction(2018, birthday)))
print('Your name in uppercase is {}.'.format(uppercase(name)))
