"""
I am a module that provides functions for text processing.
"""

def uppercase(text):
    return text.upper()

def lowercase(text):
    return text.lower()
